package com.evaluation.dto;

import lombok.Data;

/**
 * @author: xieyong
 * @date: 2019/4/25 22:57
 * @Description:
 */
@Data
public class PJDTO {

    private Integer id;

    private Integer zongfen;

    private String shijian;

    public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getZongfen() {
		return zongfen;
	}

	public void setZongfen(Integer zongfen) {
		this.zongfen = zongfen;
	}

	public String getShijian() {
		return shijian;
	}

	public void setShijian(String shijian) {
		this.shijian = shijian;
	}

	public String getTeacherName() {
		return teacherName;
	}

	public void setTeacherName(String teacherName) {
		this.teacherName = teacherName;
	}

	public String getStudentName() {
		return studentName;
	}

	public void setStudentName(String studentName) {
		this.studentName = studentName;
	}

	private String teacherName;

    private String studentName;

}
